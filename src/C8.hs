{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
module C8 where

import Control.Applicative (ZipList(..), Applicative (..))
import Control.Lens
import Control.Monad (guard)
import Control.Monad.State
import Data.Bitraversable
import Data.Char (toUpper, isAlpha, toLower)
import Data.Either.Validation
import Data.Ord (comparing)
import Data.Tree (Tree(..))
import Text.Read (readMaybe)
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text as T
import Data.Maybe (isJust)
import Control.Arrow (second)

e179_1 = ["Larry", "Curly", "Moe"]
  & ix 1 .~ "Wiggly"

heroesAndVillains = M.fromList
  [ ("Superman", "Lex")
  , ("Batman", "Joker")
  ]

e179_2 = (heroesAndVillains & at "Spiderman" .~ Just "Goblin")
  == heroesAndVillains <> M.singleton "Spiderman" "Goblin"

e179_3 = sans "Superman" heroesAndVillains == M.fromList [("Batman", "Joker")]

e179_4 = (
  S.fromList "aeiou"
    & at 'y' ?~ ()
    & at 'i' .~ Nothing
  ) == S.fromList "aeouy"

e180_1 = (
  e180_input
    & at "gum" .~ Nothing
    & at "soda" ?~ 37
    & at "ice cream" ?~ 5
  ) == e180_output
  where
    e180_input  = M.fromList [("candy bars", 13), ("soda", 34), ("gum", 7)]
    e180_output = M.fromList [("candy bars", 13), ("ice cream", 5), ("soda", 37)]

newtype Cycled a = Cycled [a]
  deriving Show
  deriving newtype Eq

type instance Index (Cycled a) = Int
type instance IxValue (Cycled a) = a

instance Ixed (Cycled a) where
  ix :: Int -> Traversal' (Cycled a) a
  ix i handler (Cycled xs) =
    Cycled <$> traverseOf (ix (i `mod` length xs)) handler xs

xs = Cycled ['a', 'b', 'c']

e180_2 = xs ^? ix 10 == Just 'b'
e180_3 = (xs & ix (-1) .~ '!') == Cycled "ab!"

data Address = Address
  { _buildingNumber  :: Maybe String
  , _streetName      :: Maybe String
  , _apartmentNumber :: Maybe String
  , _postalCode      :: Maybe String
  } deriving Show
makeLenses ''Address

data AddressPiece
  = BuildingNumber
  | StreetName
  | ApartmentNumber
  | PostalCode
  deriving Show

type instance Index Address   = AddressPiece
type instance IxValue Address = String

instance Ixed Address

instance At Address where
  at BuildingNumber  = buildingNumber
  at StreetName      = streetName
  at ApartmentNumber = apartmentNumber
  at PostalCode      = postalCode

e184_addr = Address Nothing Nothing Nothing Nothing

sherlockAddr = e184_addr
  & at StreetName      ?~ "Baker St."
  & at ApartmentNumber ?~ "221B"

e184_1 = sherlockAddr
  & ix ApartmentNumber .~ "221A"
e185_1 = sherlockAddr
  & sans StreetName

newtype E185 b = E185 { _unE :: M.Map String b }
  deriving newtype Show

makeLenses ''E185

type instance Index (E185 b)   = String
type instance IxValue (E185 b) = b

instance Ixed (E185 b)

instance At (E185 b) where
  at :: String -> Lens' (E185 b) (Maybe b)
  at i = unE . lens (snd . get) set
    where
      keq k = fmap toLower k == fmap toLower i

      mapEntry :: Traversal' (M.Map String b) (String, b)
      mapEntry = itraversed . withIndex . filtered (keq . fst)

      get m   = maybe (i, Nothing) (second Just) (m ^? mapEntry)
      set m v = m & at (fst $ get m) .~ v

safeHead :: [a] -> Maybe a
safeHead []    = Nothing
safeHead (l:_) = Just l

e191_1 = ("abc" :: String) ^. pre (ix 10) . non 'z'
e191_2 = [1, 2, 3, 4] ^. pre (traversed . filtered even)
e191_3 = [1, 3] ^. pre (traversed . filtered even)
e191_4 = [1, 3] ^. pre (traversed . filtered even) . non 0

e192_1 :: Traversal' (M.Map String Bool) Bool
e192_1 = failing (ix "first") (ix "second")
e192_1a = M.fromList [("first", False), ("second", False)] & e192_1 .~ True
e192_1b = M.fromList [("second", False)] & e192_1 .~ True

e192_2 :: Traversal' (Int, Int) Int
e192_2 = failing (_1 . filtered even) _2
e192_2a = (1, 1) & e192_2 *~ 10
e192_2b = (2, 2) & e192_2 *~ 10

-- I think the handout for this one if off...
e192_3 :: Traversal' [Int] Int
e192_3 =
  failing
    (filtered (any even) . each . filtered even)
    each

e193_1 = e == "default"
  where e = Nothing ^. non "default"
e193_2 = e == Just 107
  where e = Nothing & non 100 +~ 7
e193_3 = e == False
  where e = M.fromList [("Perogies", True), ("Pizza", True), ("Pilsners", True)]
          ^. at "Broccoli" . non False
e193_4 = e == l <> M.singleton "Wario's Woods" 999
  where
    e = l & at "Wario's Woods" . non 0 +~ 999
    l = M.fromList
      [ ("Breath of the wild", 22000000)
      , ("Odyssey", 9070000)
      ]
e193_5 = e == ("Unscheduled" :: String)
  where e = ["Math", "science", "Geography"]
          ^. pre (ix 9) . non "Unscheduled"
e193_6 = e == [-1, 2, -1, 4]
  where e = [1, 2, 3, 4]
          ^.. each . pre (filtered even) . non (-1)

module C13 where

import Control.Lens
import Control.Monad.RWS (MonadIO(..), MonadReader (..))
import Control.Monad.Reader (MonadReader, ReaderT (..))
import Control.Monad.State
import Text.Printf
import qualified Data.Map as M

type Username = String
type Password = String

data Env = Env
  { _currentUser :: Username
  , _users :: M.Map Username Password
  } deriving Show

makeLenses ''Env

printUser :: (MonadReader Env m, MonadIO m) => m ()
printUser = do
  user <- view currentUser
  mPass <- preview (users . ix user)
  liftIO . putStrLn $ "Current user: " <> user

sampleEnv = Env "jenkins" (M.singleton "jenkins" "hunter2")

e271_1 :: IO ()
e271_1 = runReaderT printUser sampleEnv

e271_2 :: IO ()
e271_2 = flip runReaderT sampleEnv $ do
  user <- view currentUser
  mPass <- preview (users . ix user)
  liftIO $ print mPass

data Till = Till
  { _total :: Double
  , _sales :: [Double]
  , _taxRate :: Double
  } deriving Show

makeLenses ''Till

e273_1 :: IO Till
e273_1 = flip execStateT (Till 0 [] 1.11) $ do
  total .= 0
  total += 8.55 -- Delicious Hazy IPA
  total += 7.36 -- Red Grapefruit Sour
  totalSale <- use total
  liftIO $ printf "Total sale: $%.2f\n" totalSale

e274_1 :: IO Till
e274_1 = flip execStateT (Till 0 [] 1.11) $ do
  total .= 0
  total += 8.55 -- Delicious Hazy IPA
  total += 7.36 -- Red Grapefruit Sour
  totalSale <- use total
  liftIO $ printf "Total sale: $%.2f\n" totalSale
  sales <>= [totalSale]
  total <~ uses taxRate (totalSale *)
  taxIncluded <- use total
  liftIO $ printf "Tax included: $%.2f\n" taxIncluded

---

data Weather = Weather
  { _temperature :: Float
  , _pressure :: Float
  } deriving Show

makeLenses ''Weather

printData :: String -> ReaderT Float IO ()
printData statName = do
  num <- ask
  liftIO . putStrLn $ statName <> ": " <> show num

weatherStats :: ReaderT Weather IO ()
weatherStats = do
  magnify temperature (printData "temp")
  magnify pressure (printData "pressure")

e276_1 :: IO ()
e276_1 = runReaderT weatherStats (Weather 15 7.2)

cToF :: Iso' Float Float
cToF = iso celsiusToF fahrenheitToC
  where celsiusToF c    = (c * (9/5)) + 32
        fahrenheitToC f = (f - 32) * 5/9

celsiusToFahrenheit :: StateT Float IO ()
celsiusToFahrenheit = modify (view cToF)

weatherStats' :: StateT Weather IO ()
weatherStats' = do
  zoom temperature celsiusToFahrenheit

e276_2 :: IO Weather
e276_2 = execStateT weatherStats' (Weather 32 12)

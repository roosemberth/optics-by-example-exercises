{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
module C6 where

import Control.Applicative (ZipList(..), Applicative (..))
import Control.Lens
import Control.Monad (guard)
import Control.Monad.State
import Data.Bitraversable
import Data.Char (toUpper, isAlpha, toLower)
import Data.Either.Validation
import Data.Ord (comparing)
import Data.Tree (Tree(..))
import Text.Read (readMaybe)
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text as T
import Data.Maybe (isJust)
import Control.Arrow (second)

data Role
  = Gunner
  | PowderMonkey
  | Navigator
  | Captain
  | FirstMate
  deriving (Show, Eq, Ord)

data CrewMember = CrewMember
  { _name :: String
  , _role :: Role
  , _talents :: [String]
  } deriving (Show, Eq, Ord)
makeLenses ''CrewMember

roster :: S.Set CrewMember
roster = S.fromList
  [ CrewMember "Grumpy Roget"     Gunner       ["Juggling", "Arbitrage"]
  , CrewMember "Long-John Bronze" PowderMonkey ["Origami"]
  , CrewMember "Salty Steve"      PowderMonkey ["Charcuterie"]
  , CrewMember "One-eyed Jack"    Navigator    []
  ]

rosterRoles :: Fold (S.Set CrewMember) Role
rosterRoles = folded . role

e81_1 = roster ^.. rosterRoles
e82_2 = roster & toListOf rosterRoles

e83_1 = Just "Buried Treasure" ^.. folded
e83_2 = Nothing ^.. folded
e83_3 = Identity "Cutlass" ^.. folded
e83_4 = ("Rubies", "Gold") ^.. folded
e83_5 =
      M.fromList [("Jack", "Captain"), ("Will", "First Mate")]
  ^.. folded
e85_1 = ("Gemini", "Leo") ^.. both
e85_2 = ("Gemini", "Leo", "Libra") ^.. both

-- e86_1
beastSizes :: [(Int, String)]
beastSizes = [(3, "Sirens"), (882, "Kraken"), (92, "Ogopongo")]

e86_1a = beastSizes ^.. folded == beastSizes
e86_1b = beastSizes ^.. folded . folded == ["Sirens", "Kraken", "Ogopongo"]
e86_1c = beastSizes ^.. folded . folded . folded == "SirensKrakenOgopongo"
e86_1d = beastSizes ^.. folded . _2 == ["Sirens", "Kraken", "Ogopongo"]
e86_1e = toListOf (folded . folded) [[1, 2, 3], [4, 5, 6]] == [1, 2, 3, 4, 5, 6]
e86_1f =
  toListOf
    (folded . folded)
    (M.fromList [("Jack", "Captain" :: String), ("Will", "First Mate")])
  == "CaptainFirst Mate"
e86_1g = ("Hello" :: String, "It's me") ^.. both . folded == "HelloIt's me"
e86_1h = ("Why", "So", "Serious?") ^.. each == ["Why", "So", "Serious?"]

quotes :: [(T.Text, T.Text, T.Text)]
quotes = [("Why", "So", "Serious"), ("This", "is", "SPARTA")]

e86_1i = quotes ^.. each . each . each == "WhySoSeriousThisisSPARTA"

e87_2a :: (Foldable (t a), Bitraversable t) => Fold (t a a) a
e87_2a = folded

e87_2b :: Fold (a, b) a
e87_2b = _1

e87_2c :: Fold [(a, b)] a
e87_2c = folded . _1
e87_2c' = toListOf e87_2c [(1, 'a'), (2, 'b'), (3, 'c')]

e87_2d :: Foldable t => Fold (a, t b) b
e87_2d = _2 . folded
e87_2d' = toListOf e87_2d (False, S.fromList ["one", "two", "three"])

e87_2e :: Foldable t => Fold (t [a]) a
e87_2e = folded . folded
e87_2e' = toListOf e87_2e
    (M.fromList [("Jack", "Captain" :: String), ("Will", "First Mate")])

e87_3a = [1, 2, 3] ^.. each == [1, 2, 3]
e87_3b = ("Light", "Dark") ^.. _1 == ["Light"]
e87_3c = [("Light", "Dark"), ("Happy", "Sad")] ^.. folded . each
  == ["Light", "Dark", "Happy", "Sad"]
e87_3d= [("Light", "Dark"), ("Happy", "Sad")] ^.. folded . _1
  == ["Light", "Happy"]
e87_3e = [("Light", "Dark"), ("Happy", "Sad")] ^.. folded . _2 . (folded @[])
  == "DarkSad"
e87_3f = ("Bond", "James", "Bond") ^.. each == ["Bond", "James", "Bond"]

-- e88

newtype Name = Name
  { getName :: String
  } deriving Show

data ShipCrew = ShipCrew
  { _shipName :: Name
  , _captain :: Name
  , _firstMate :: Name
  , _conscripts :: [Name]
  } deriving Show
makeLenses ''ShipCrew

collectCrewMembers :: ShipCrew -> [Name]
collectCrewMembers ShipCrew{..} = [_captain, _firstMate] ++ _conscripts

crewMembers :: Fold ShipCrew Name
crewMembers = folding collectCrewMembers

myCrew :: ShipCrew
myCrew =
  ShipCrew
    { _shipName   = Name "Purple Pearl"
    , _captain    = Name "Gumpy Roger"
    , _firstMate  = Name "Long-John Bronze"
    , _conscripts = [Name "One-eyed Jack", Name "Filthy Frank"]
    }

e89_1 = myCrew ^.. crewMembers

e91_1 = Name "Two-faced Tony" ^. to getName
e91_2 = Name "Two-faced Tony" ^. to getName . to (fmap toUpper)
e91_3 = Name "Two-faced Tony" ^. to (fmap toUpper . getName)
e91_4 = myCrew ^.. crewMembers . to getName

crewMembers' :: Fold ShipCrew Name
crewMembers' = folding $
  \s -> s ^.. captain
     <> s ^.. firstMate
     <> s ^.. conscripts . folded

e93_1a = ["Yer" :: String, "a", "wizard", "Harry"] ^.. folded . folded
  == "YerawizardHarry"
e93_1b = [[1, 2, 3], [4, 5, 6]] ^.. folded . folding (take 2)
  == [1, 2, 4, 5]
e93_1c = [[1, 2, 3], [4, 5, 6]] ^.. folded . to (take 2)
  == [[1, 2], [4, 5]]
e93_1d = ["bob", "otto", "hannah"] ^.. folded . to reverse
  == ["bob", "otto", "hannah"]
e93_1e = ("abc" :: String, "def") ^..
      folding (\(a, b) -> [a, b])
    . to reverse
    . folded
  == "cbafed"

e93_2a = [1..5] ^.. folded . to (* 100) == [100, 200, 300, 400, 500]
e93_2b = (1, 2) ^.. each == [1, 2]
e93_2c = [(1, "one" :: String), (2, "two")] ^.. each . folded == ["one", "two"]
e93_2d = (Just 1, Just 2, Just 3) ^.. each . folded == [1, 2, 3]
e93_2e = [Left 1, Right 2, Left 3] ^.. each . folded == [2]
e93_2f = [([1, 2], [3, 4]), ([5, 6], [7, 8])] ^.. each . each . each == [1..8]
e93_2g = [(Just 1, Left ("one" :: String)), (Nothing, Right 2)] ^.. each . nums
  == [1, 2]
  where nums = folding (\s -> s ^.. _1 . folded <> s ^.. _2 . folded)
e93_2h = [(1, "one"), (2, "two")] ^.. each . classify
  == [Left 1, Right "one", Left 2, Right "two"]
  where classify = folding (\s -> s ^.. _1 . to Left <> s ^.. _2 . to Right)
e93_2i = S.fromList ["apricots", "apples" :: String]
  ^.. folded . to reverse . folded
  == "selppastocirpa"
e93_3a = [(12, 45, 66), (91, 123, 87)]
  ^.. each . _2 . to show . to reverse . folded
  == "54321"
e93_3b = [(1, "a"), (2, "b"), (3, "c"), (4, "d")]
  ^.. each . filtered (^. _1 . to even) . _2
  == ["b", "d"]

e95_1 = elemOf folded 3 [1, 2, 3, 4]
e95_2 = elemOf folded 99 [1, 2, 3, 4]
e95_3 = anyOf folded even [1, 2, 3, 4]
e95_4 = anyOf folded (>10) [1, 2, 3, 4]
e96_1 = allOf folded even [1, 2, 3, 4]
e96_2 = allOf folded (<10) [1, 2, 3, 4]
e96_3 = findOf folded even [1, 2, 3, 4]
e96_4 = findOf folded (>10) [1, 2, 3, 4]
e96_5 = has folded []
e96_6 = has folded [1, 2, 3, 4]
e96_7 = hasn't folded []
e96_8 = hasn't folded [1, 2, 3, 4]
e96_9 = lengthOf folded [1, 2, 3, 4]
e96_10 = sumOf folded [1, 2, 3, 4]
e96_11 = productOf folded [1, 2, 3, 4]
e97_1 = firstOf folded []
e97_2 = firstOf folded [1, 2, 3, 4]
e97_3 = preview folded [1, 2, 3, 4]
e97_4 = [1, 2, 3, 4] ^? folded
e97_5 = lastOf folded [1, 2, 3, 4]
e98_1 = minimumOf folded [2, 1, 4, 3]
e98_2 = maximumOf folded [2, 1, 4, 3]
e98_3 = minimumOf folded ([] @Int)
e98_4 = maximumOf folded ([] @Int)

-- name was renamed to name' because of a conflict with the attribute of
-- CrewMember.

data Actor = Actor
  { _name' :: String
  , _birthYear :: Int
  } deriving (Eq, Show)
makeLenses ''Actor

data TVShow = TVShow
  { _title :: String
  , _numEpisodes :: Int
  , _numSeasons :: Int
  , _criticScore :: Double
  , _actors :: [Actor]
  } deriving (Eq, Show)
makeLenses ''TVShow

howImetYourMother :: TVShow
howImetYourMother = TVShow
  { _title = "How I Met Your Mother"
  , _numEpisodes = 208
  , _numSeasons = 9
  , _criticScore = 83
  , _actors =
    [ Actor "Josh Radnor" 1974
    , Actor "Cobie Smulders" 1982
    , Actor "Neil Patric Harris" 1973
    , Actor "Alyson Hannigan" 1974
    , Actor "Jason Segel" 1980
    ]
  }

buffy :: TVShow
buffy = TVShow
  { _title = "Buffy the Vampire Slayer"
  , _numEpisodes = 144
  , _numSeasons = 7
  , _criticScore = 81
  , _actors =
    [ Actor "Sarah Michelle Gallar" 1977
    , Actor "Alyson Hannigan" 1974
    , Actor "Nicholas Brendon" 1971
    , Actor "David Boreanaz" 1969
    , Actor "Anthony Head" 1954
    ]
  }

tvShows :: [TVShow]
tvShows = [howImetYourMother, buffy]

e99_1 = sumOf (folded . numEpisodes) tvShows
e100_1 = maximumOf (folded . criticScore) tvShows

e100_2 = _title <$> maximumByOf folded (comparing _criticScore) tvShows
e100_3 = minimumByOf (folded . actors . folded) (comparing _birthYear) tvShows

comparingOf :: Ord b => Lens' a b -> (a -> a -> Ordering)
comparingOf l = comparing (view l)

e101_1 = minimumByOf (folded . actors . folded) (comparingOf birthYear) tvShows

calcAge :: Actor -> Int
calcAge actor = 2030 - _birthYear actor

showActor :: Actor -> String
showActor actor = _name' actor <> ": " <> show (calcAge actor)

e102_1 = traverseOf_
  (folded . actors . folded . to showActor)
  putStrLn
  tvShows

data Average = Average { _sum :: Int, _count :: Int }
  deriving (Eq, Show)

unAverage :: Fractional a => Average -> a
unAverage Average{..} =
  if _count == 0 then 0 else fromIntegral _sum / fromIntegral _count

intoAverage :: Int -> Average
intoAverage v = Average v 1

instance Semigroup Average where
  (Average s1 c1) <> (Average s2 c2) = Average (s1 + s2) (c1 + c2)

instance Monoid Average where
  mempty = Average 0 0

avgActorAge = unAverage $ foldOf
  (folded . actors . folded . to calcAge . to intoAverage)
  tvShows

-- List of actors without duplicates
allActors = M.keys $ foldMapOf
  (folded . actors . folded . name') (`M.singleton` 1) tvShows

actorsWithNumShows =
  foldMapByOf
    (folded . actors . folded . name')
    (M.unionWith (+))
    mempty
    (`M.singleton` 1)
    tvShows

e109_1a = has folded [] == sln
  where sln = False
e109_1b = foldOf both ("Yo", "Adrian!") == "YoAdrian!"
e109_1c = elemOf each "phone" ("E.T.", "phone", "home") == sln
  where sln = True
e109_1d = ans folded [5, 7, 2, 3, 13, 17, 11] == Just 2
  where ans = flip findOf even
e109_1e = lastOf folded [5, 7, 2, 3, 13, 17, 11] == Just 11
e109_1f =
  anyOf
    folded
    ((> 9) . length @[])
    ["Bulbasaur", "Charmander", "Squirtle"] == sln
  where sln = True
e109_1g = findOf folded even [11, 22, 3, 5, 6] == Just 22
e109_1 = and [e109_1a, e109_1b, e109_1c, e109_1d, e109_1e, e109_1f, e109_1g]

e109_2a =
  findOf
    folded
    (\e -> e == reverse e)
    ["umbrella", "olives", "racecar", "hammer"] == Just "racecar"
e109_2b = allOf each even (2, 4, 6) == sln
  where sln = True
e109_2c =
  maximumByOf
    folded
    (comparing (^. _1))
    [(2, "I'll"), (3, "Be"), (1, "Back")] == Just (3, "Be")
e109_2d = foldByOf each (+) 0 (1, 2) == 3
e109_2 = and [e109_2a, e109_2b, e109_2c, e109_2d]

e110_3a =
  maximumByOf
    (folding words)
    (comparing length)
    "Do or do not, there is no try." == Just "there"
e110_3b = foldOf (reversed . folded) ["a", "b", "c"] == "cba"
e110_3c =
  foldOf
    (folded . _2 . to show . to reverse)
    [(12, 45, 66), (91, 123, 87)] == "54321"
e110_3d =
  foldOf
    (folded . filtered (even . view _1) . _2 . to (pure @[]))
    [(1, "a"), (2, "b" :: String), (3, "c"), (4, "d")] == ["b", "d"]
e110_3 = e110_3a && e110_3b && e110_3c

e114_1 = [[1, 2, 3], [10, 20, 30], [100, 200, 300]]
  ^.. folded . each
  ^.. taking 2 folded == [1, 2]
e114_2 = [[1, 2, 3], [10, 20, 30], [100, 200, 300]]
  ^.. folded . taking 2 folded == [1, 2,10, 20, 100, 200]
e114_3 = [[1, 2, 3], [10, 20, 30], [100, 200, 300]]
  ^.. folded . dropping 2 folded == [3, 30, 300]
e114_4 = [[1, 2, 3], [10, 20, 30], [100, 200, 300]]
  ^. dropping 1 folded == [10, 20, 30, 100, 200, 300]
e114_5 = [[1, 2, 3], [10, 20, 30], [100, 200, 300]]
  ^.. each . to (toListOf $ dropping 1 each)
  ^.. each . taking 1 each == [2, 20, 200]

e114_6 = [[1, 2, 3], [10, 20, 30], [100, 200, 300]]
  ^.. each . dropping 1 each
  ^.. dropping 1 each == [3, 20, 30, 200, 300]
e114_7 = [[1, 2, 3], [10, 20, 30], [100, 200, 300]]
  ^.. dropping 5 (each . each) == [30, 100, 200, 300]

e115_1 = [[1, 2, 3], [10, 20, 30], [100, 200, 300]]
  ^.. dropping 5 (backwards each . backwards each) == [10, 3, 2, 1]
e115_2 = [[1, 2, 3], [10, 20, 30], [100, 200, 300]]
  ^.. backwards each . dropping 2 (backwards each) == [100, 10, 1]

e117_1a = ("Here's looking at you, kid" :: String) ^.. dropping 7 folded
  == "looking at you, kid"
e117_1b = ["My Precious", "Hakuna Matata", "No problemo"]
  ^.. folded . taking 1 (folding words) == ["My", "Hakuna", "No"]
e117_1c = ["My Precious", "Hakuna Matata", "No problemo"]
  ^.. taking 1 (folded . folding words) == ["My"]
e117_1d = ["My Precious", "Hakuna Matata", "No problemo"]
  ^.. folded . taking 1 (folding words) . each == "MyHakunaNo"
e117_1e = ["My Precious" :: String, "Hakuna Matata", "No problemo"]
  ^.. folded . takingWhile isAlpha folded == "MyHakunaNo"
e117_1f = sumOf (taking 2 each) (10, 50, 100) == 60
e117_1g = ("stressed", "guns", "evil")
  ^.. backwards each == ["evil", "guns", "stressed"]
e117_1h = ("stressed", "guns", "evil")
  ^.. backwards each . to reverse == ["live", "snug", "desserts"]
e117_1i = ["blink182 k9 blazeit420" :: String]
  ^.. each . each . filtered (not . isAlpha) . filtered (/= ' ') == "1829420"
e117_1 = and
  [ e117_1e, e117_1b, e117_1c, e117_1d, e117_1e, e117_1f, e117_1g, e117_1h
  , e117_1i]

sample = [-10, -5, 4, 3, 8, 6, -2, 3, -5, -7]

-- The exercise asks for '2', but it is not guaranteed that any measurements
-- were above 0.
e117_2a = findIndexOf folded (> 0) sample == Just 2

e117_2b = maximumOf (taking 4 folded) sample == Just 4
e117_2c = sample ^? dropping 1 (droppingWhile (/= 4) each) == Just 3
e117_2d = lengthOf (backwards (takingWhile (< 0) each)) sample == 2
e117_2e = sample
  ^.. droppingWhile (< 0) each
  ^.. takingWhile (> 0) each == [4, 3, 8, 6]

trimmingWhile :: (a -> Bool) -> Fold s a -> Fold s a
trimmingWhile p f =
    droppingWhile p
  . backwards
  . droppingWhile p
  . backwards $ f

e117_2f = sample ^.. trimmingWhile (< 0) each == [4, 3, 8, 6, -2, 3]
e117_2 = and [e117_2a, e117_2b, e117_2c, e117_2d, e117_2e]

data Aura = Wet | Hot | Spark | Leafy deriving (Show, Eq)

data Move = Move
  { _moveName :: String
  , _movePower :: Int
  } deriving (Show, Eq)

data Card = Card
  { _name'' :: String
  , _aura :: Aura
  , _holo :: Bool
  , _moves :: [Move]
  } deriving (Show, Eq)

makeLenses ''Card
makeLenses ''Move

deck :: [Card]
deck =
  [ Card "Skwortul"    Wet   False [Move "Squirt" 20]
  , Card "Scorchander" Hot   False [Move "Scorch" 20]
  , Card "Seedasaur"   Leafy False [Move "Allergize" 20]
  , Card "Kapichu"     Spark False [Move "Poke" 10 , Move "Zap" 30]
  , Card "Elecdude"    Spark False [Move "Asplode" 50]
  , Card "Garydose"    Wet   True  [Move "Gary's move" 40]
  , Card "Moisteon"    Wet   False [Move "Soggy" 3]
  , Card "Grasseon"    Leafy False [Move "Leaf Cut" 30]
  , Card "Spicyeon"    Hot   False [Move "Capsaicisize" 40]
  , Card "Sparkeon"    Spark True  [Move "Shock" 40 , Move "Battery" 50]
  ]

e121_1 = lengthOf (folded . aura . filtered (== Spark)) deck
e121_2 = lengthOf
  ( folded         -- Over each card
  . moves          -- Select moves
  . folded         -- Over each move
  . movePower      -- select power
  . filtered (>30) -- Filter power > 30
  ) deck
e121_3 = deck
  ^.. folded
    . filtered (anyOf (moves . folded . movePower) (> 40))
    . name''

e122_1 = lengthOf
  ( folded
  . filtered ((== Spark) . view aura)
  . moves
  . folded
  ) deck

e122_2 = deck
  ^.. folded
    . filteredBy (aura . only Spark)
    . moves
    . folded
    . filteredBy (movePower. filtered (> 30))
    . moveName

e124_1 = deck &
  maximumByOf
    (folded . filteredBy holo)
    (comparing $ lengthOf moves)

e125_1a = deck
  ^.. folded
    . filteredBy (name'' . taking 1 each . only 'S')
e125_1b = deck &
  minimumByOf
   ( folded
   . moves
   . folded
   ) (comparing $ view movePower)
e125_1c = deck
  ^? folded
   . filtered ((> 1) . length . view moves)
   . name''
e125_1d = deck
  & anyOf
    ( folded
    . filteredBy (aura . only Hot)
    . moves
    . folded
    . movePower
    ) (> 30)
e125_1e = deck
  ^.. folded
    . filtered (view holo)
    . filteredBy (aura . only Wet)
    . name''
e125_1f = deck &
  sumOf
    ( folded
    . filtered ((/= Leafy) . view aura)
    . moves
    . folded
    . movePower
    )

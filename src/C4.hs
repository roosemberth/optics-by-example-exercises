{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE DataKinds #-}
module C4 where

import Control.Lens

data Promotion a = Promotion
  { _item :: a
  , _discountPercentage :: Double
  } deriving Show

item :: Lens (Promotion a) (Promotion b) a b
item = lens getter setter
  where
    getter :: Promotion a -> a
    getter = _item
    setter :: Promotion a -> b -> Promotion b
    setter promo newItem = promo{_item = newItem}

peachPromo = Promotion "A really delicious Peach" 25.0

buffyFigurines = ["Buffy", "Angel", "Willow", "Giles"]

buffyPromo = set item buffyFigurines peachPromo

data Preferences a = Preferences
  { _best :: a
  , _worst :: a
  } deriving Show

newtype Vorpal a = Vorpal a

e50_1 :: Lens (Vorpal x) (Vorpal y) x y
e50_1 = lens getter setter
  where
    getter (Vorpal a) = a
    setter _ = Vorpal

--e51_2

best :: Lens (Preferences a) (Preferences (Either a b)) a b
best = lens getter setter
  where
    getter = _best
    setter :: Preferences a -> b -> Preferences (Either a b)
    setter (_worst -> worst) newBest =
      Preferences{_worst = Left worst, _best = Right newBest}

worst :: Lens (Preferences a) (Preferences (Either a b)) a b
worst = lens getter setter
  where
    getter = _worst
    setter :: Preferences a -> b -> Preferences (Either a b)
    setter (_best -> best) newWorst =
      Preferences{_worst = Right newWorst, _best = Left best}

-- e51_3
data Result e = Result
  { _lineNumber :: Int
  , _result :: Either e String
  } deriving Show

e51_3 :: Lens (Result e) (Result e2) (Either e String) (Either e2 String)
e51_3 = lens getter setter
  where
    getter = _result
    setter r _result = r{_result}

-- e51_4: It is not: By definition, a lens should focus on a single thing.
-- Such a thing can only have one type...

newtype Predicate a = Predicate { _unPred :: a -> Bool}

e51_5 :: Lens (Predicate a) (Predicate b) (a -> Bool) (b -> Bool)
e51_5 = lens getter setter
  where
    getter = _unPred
    setter _ = Predicate

data Person = Person
  { _name :: String
  , _address :: Address
  } deriving Show

data Address = Address
  { _streetAddress :: StreetAddress
  , _city :: String
  , _country :: String
  } deriving Show

data StreetAddress = StreetAddress
  { _streetNumber :: String
  , _streetName :: String
  } deriving Show

makeLenses ''Person
makeLenses ''Address
makeLenses ''StreetAddress

sherlock :: Person
sherlock =
  Person "S. Holmes"
    $ Address
      (StreetAddress "221B" "Baker Street")
        "London"
        "England"

sherlock' = set (address . streetAddress . streetNumber) "221A" sherlock

type Modifier a = a -> a
type Updater a s = Modifier a -> Modifier s

data Player  = Player deriving Show
data Wool    = Wool deriving Show
data Sweater = Sweater deriving Show

data Item a = Item
  { _material :: a
  , _amount :: Int
  } deriving Show

makeLenses ''Item

weave :: Wool -> Sweater
weave Wool = Sweater

gameState :: (Player, Item Wool)
gameState = (Player, Item Wool 5)

e61_1 = over (_2 . material) weave gameState

e62_1 = view (_2 . _1 . _2) ("Ginerva", (("Galileo", "Waldo"), "Malfoy"))

-- e62_2

data Five
data Eight
data Two
data Three

fiveEightDomino :: Lens' Five Eight
fiveEightDomino = undefined
mysteryDomino :: Lens' Eight Two
mysteryDomino = undefined
twoThreeDomino :: Lens' Two Three
twoThreeDomino = undefined

dominoTrain :: Lens' Five Three
dominoTrain = fiveEightDomino . mysteryDomino . twoThreeDomino

-- e62_3
data Armadillo
data Hedgehog
data Platypus
data BabySloth

e62_3 :: Functor f => (Armadillo -> f Hedgehog) -> (Platypus -> f BabySloth)
e62_3 = undefined :: Lens Platypus BabySloth Armadillo Hedgehog

-- e62_4

data Bandersnatch
data Boojum
data Chumble
data Foob
data Gazork
data Grug
data Jabberwock
data JubJub
data Mog
data Pubbawup
data Snark
data Spuzz
data Trowlg
data Wattoom
data Yakka
data Zink

snajubjumwock     :: Lens Snark JubJub Boojum Jabberwock   = undefined
boowockugwup      :: Lens Boojum Jabberwock Grug Pubbawup  = undefined
gruggazinkoom     :: Lens Grug Pubbawup Zink Wattoom       = undefined
zinkattumblezz    :: Lens Zink Wattoom Chumble Spuzz       = undefined
spuzorktrowmble   :: Lens Chumble Spuzz Gazork Trowlg      = undefined
gazorlglesnatchka :: Lens Gazork Trowlg Bandersnatch Yakka = undefined
banderyakoobog    :: Lens Bandersnatch Yakka Foob Mog      = undefined

e62_4 :: Lens Snark JubJub Foob Mog
e62_4 = snajubjumwock
      . boowockugwup
      . gruggazinkoom
      . zinkattumblezz
      . spuzorktrowmble
      . gazorlglesnatchka
      . banderyakoobog

{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
module C7 where

import Control.Applicative (ZipList(..), Applicative (..))
import Control.Lens
import Control.Monad (guard)
import Control.Monad.State
import Data.Bitraversable
import Data.Char (toUpper, isAlpha, toLower)
import Data.Either.Validation
import Data.Ord (comparing)
import Data.Tree (Tree(..))
import Text.Read (readMaybe)
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text as T
import Data.Maybe (isJust)
import Control.Arrow (second)

e129_1 = (1, 2, 3) & each %~ (*10)
e129_2 = [1, 2, 3] & each %~ (*10)
e129_3 = ("Here's Jonny" :: T.Text) & each %~ toUpper

e130_1 = ("once upon a time - optics became mainstream" :: String)
  & takingWhile (/= '-') traversed
  %~ toUpper
e130_2 = [1, 2, 3, 4, 5]
  & traversed . filtered even
  *~ 10

powerLevels = M.fromList
  [ ("Gohan", 710)
  , ("Goku", 9001)
  , ("Krillin", 5000)
  , ("Piccolo", 408)
  ]
e132_1 = powerLevels
  & traversed %~ \n ->
    if n > 9000
    then "Over 9000"
    else show n

opticsTree = Node "Lens" [Node "Fold" [], Node "Traversal" []]
e132_2 = opticsTree & traversed %~ reverse

numbers = ([(1, 2), (3, 4)], [5, 6, 7])
e134_1 = numbers ^.. beside (traversed . both) traversed

e136_1 = [0..4] ^? elementOf traversed 2
e136_2 = [[0, 1, 2], [3, 4], [5, 6, 7, 8]] ^? elementOf (traversed . traversed) 6

e137_1 = ("short", "really long")
  & traversed
  . filtered ((> 5) . length)
  . worded
  %~ \s -> "*" <> s <> "*"
e137_2 = (("Ritchie", 100000), ("Archie", 32), ("Reggie", 4350))
  & each
  . filtered ((> 1000) . snd)
  . _1
  %~ ("Rich " <>)

{- e137:

1) A fold: A traversal can set values, a fold cannot.
2) both
3) folded
-}

e138_2a = ("Jurassic", "Park") & both .~ "N/A"
e138_2b = ("Jurassic" :: String, "Park") & both . traversed .~ 'x'
e138_2c = ("Malcolm", ["Kaylee", "Inara", "Jayne"])
  & beside id traversed %~ take 3
e138_2d = ("Malcolm", ["Kaylee", "Inara", "Jayne"])
  & _2 . element 1 .~ "River"
e138_2e = ["Die Another Day", "Live and Let Die", "You Only Live Twice"]
  & traversed
  . elementOf worded 1
  . traversed
  .~ 'x'
e138_2f = ((1, 2), (3, 4)) & each . each +~ 1
e138_2g = (1, (2, [3, 4])) & beside id (beside id traversed) +~ 1
e138_2h = ((True, "Strawberries"), (False, "Blueberries"), (True, "Blackberries" :: String))
  & each
  . filtered fst
  . _2
  . taking 5 traversed
  %~ toUpper
e138_2i = ((True, "Strawberries"), (False, "Blueberries"), (True, "Blackberries"))
  & each
  %~ snd

e142_1 = [1..9]
  & traverseOf
    each
    (\v -> guard (v < 10) >> Just v)
e143_1 = ('a', 'b')
  & traverseOf both (\c -> [toLower c, toUpper c])

-- XXX: Where is the Traversable instance coming from? This yields
-- `Just ('A', 'B')` "as expected", but what Traversable instance did it use?
e143_1' = ('a', 'b')
  & traverseOf both (Just . toUpper)

e144_1 = ("ab", "cd" :: String)
  & traverseOf (both . traversed) (\c -> [toLower c, toUpper c])

validateEmail :: String -> Either String String
validateEmail email | '@' `elem` email = Right email
                    | otherwise        = Left ("missing '@': " <> email)

e144_2 = traverseOf (traversed . _2) validateEmail
  [ ("Mike", "mike@tmnt.io")
  , ("Raph", "raph.io")
  , ("Don", "don@tmnt.io")
  , ("Leo", "leo@tmnt.io")
  ]

validateEmail' :: String -> Validation [String] String
validateEmail' email | '@' `elem` email = Success email
                     | otherwise        = Failure ["missing '@': " <> email]

e145_1 = validateEmail' & forOf (traversed . _2)
  [ ("Mike", "mike@tmnt.io")
  , ("Raph", "raph.io")
  , ("Don", "don@tmnt.io")
  , ("Leo", "tmnt.io")
  ]
e145_2 = sequenceAOf _1 (Just "Garfield", "Lasagna")

e146_1 = (("1", "2") & both %%~ readMaybe) :: Maybe (Int, Int)

e147_1a = sequenceOf _1 (Nothing, "Rosebud")
e147_1b = sequenceAOf (traversed . _1) [("ab" :: String, 1), ("cd", 2)]
e147_1c = sequenceAOf traversed [ZipList [1, 2], ZipList [3, 4]]
  == ZipList [[1, 3], [2, 4]]
e147_1d =
  sequenceAOf (traversed . _2) [('a', ZipList [1, 2]), ('b', ZipList [3, 4])]
  == ZipList [[('a',1),('b',3)],[('a',2),('b',4)]]
e147_1e = runState result 0 == (([1, 2, 3], (4, 5)), 5)
  where result = traverseOf
          (beside traverse each)
          (\n -> modify (+n) >> get)
          ([1, 1, 1], (1, 1))
e147_1e' = runState result 0 == (([1, 2, 3], (4, 5)), 5)
  where result = ([1, 1, 1], (1, 1))
          & beside traverse each
          %%~ (\n -> modify (+n) >> get)

e148_2 =
  ("ab" :: String, True) & (_1 . traversed) %%~ \c -> [toLower c, toUpper c]

data User = User
  { _name''' :: String
  , _age :: Int
  } deriving Show
makeLenses ''User

data Account = Account
  { _txId :: String
  , _user :: User
  } deriving Show
makeLenses ''Account

newtype FailLeft b = FailLeft { unFail :: Either String b }
  deriving newtype (Functor, Applicative, Monad)

instance MonadFail FailLeft where
  fail = FailLeft . Left

validateAge :: Account -> Either String Account
validateAge = user . age %%~ \a -> unFail $ do
  when (a > 150)
    (fail $ "User outside the accepted age parameters: " <> show a)
  unless (a > 0)
    (fail $ "The user age is not above zero: " <> show a)
  pure a

values :: Applicative f => (a -> f b) -> [a] -> f [b]
values _ [] = pure []
values handler (a : as) = (:) <$> handler a <*> values handler as

data Transaction =
    Withdrawal  {_amount :: Int}
  | Deposit     {_amount :: Int}
  deriving Show
makeLenses ''Transaction

newtype BankAccount = BankAccount
  { _transactions :: [Transaction]
  } deriving Show
makeLenses ''BankAccount

aliceAccount = BankAccount [Deposit 100, Withdrawal 20, Withdrawal 10]

deposits :: Applicative f => (Int -> f Int) -> [Transaction] -> f [Transaction]
deposits _ []                   = pure []
deposits f (t@(Withdrawal _):l) = (t:) <$> deposits f l
deposits f ((Deposit t):l)      = (:) <$> fmap Deposit (f t) <*> deposits f l

-- e156_1

amountT :: Traversal' Transaction Int
amountT h (Withdrawal v) = fmap Withdrawal (h v)
amountT h (Deposit v)    = fmap Deposit    (h v)

-- e156_2

both' :: Traversal (a, a) (b, b) a b
both' h (v1, v2) = (,) <$> h v1 <*> h v2

-- e156_3

transactionDelta :: Traversal' Transaction Int
transactionDelta h (Withdrawal v) = fmap (Withdrawal . negate) (h $ negate v)
transactionDelta h (Deposit v)    = fmap Deposit (h v)

-- e156_4

left' :: Traversal (Either a b) (Either a' b) a a'
left' h (Left v) = Left <$> h v
left' _ (Right v) = pure (Right v)

-- e156_5
beside' :: Traversal s t a b -> Traversal s' t' a b -> Traversal (s, s') (t, t') a b
beside' t1 t2 h (s1, s2) = (,) <$> (s1 & t1 %%~ h) <*> (s2 & t2 %%~ h)

e161_1 = (e & worded %%~ pure) /= pure @[] e -- worded breaks the second law
  where e = "foo \n bar"

e161_2 :: Traversal' [Int] Int
e161_2 h []     = pure []
e161_2 h (l:ls) = fmap (l:) $ (:) <$> h l <*> e161_2 h ls

e161_3 :: Traversal' [Int] Int
e161_3 h = traverse (\n -> (+) <$> h n <*> h n)

e161_4a = ([1..10] & taking 3 traversed %%~ (pure @[])) == pure [1..10]
-- e161_4b: I think `beside` is lawful.
-- e161_4c: I think `each` is lawful.
e161_4d = composed == composed' && composed /= chained
  where
    run = flip runState 0
    base = "Hello\nworld!"
    transform = (<> "\n From the train!")
    action v = get @Int >>= put . (+1) >> pure v
    chained = run $ base
      & lined %~ transform
      & lined %%~ action                    -- = (_, 4)
    composed = run $ base
      & lined %%~ (action . transform)      -- = (_, 2)
    composed' = run $ base
      & lined %%~ (fmap transform . action) -- = (_, 2)
-- e161_4e: I think `traversed` is lawful.


e162_1 = [('a', 1), ('b', 2), ('c', 3)]
  & partsOf (traversed . _1) %~ reverse

e167_1 = [1, 2, 3, 4] ^. partsOf (traversed . filtered even)
  == [2, 4]

e167_2 = ["Aadvark", "Bandicoot", "Capybara" :: String]
  ^. traversed . partsOf (taking 3 traversed)
  == "AadBanCap"

e167_3 = ([1, 2], M.fromList [('a', 3), ('b', 4)])
  ^. partsOf (beside traversed traversed)
  == [1, 2, 3, 4]

e167_4 = ([1, 2, 3, 4] & partsOf (traversed . filtered even) .~ [20, 40])
  == [1, 20, 3, 40]

e167_5 =
  ( ["Aadvark", "Bandicoot", "Capybara" :: String]
    & partsOf (taking 1 traversed) .~ ["Kangoroo"]
  ) == ["Kangoroo", "Bandicoot", "Capybara"]

e167_6 =
  ( ["Aardvark", "Bandicoot", "Capybara" :: String]
    & partsOf (traversed . traversed) .~ "Ant"
  ) == ["Antdvark", "Bandicoot", "Capybara"]

e167_7 =
  ( M.fromList [('a', 'a'), ('b', 'b'), ('c', 'c')]
    & partsOf traversed %~ \(x:xs) -> xs ++ [x]
  ) == M.fromList [('a', 'b'), ('b', 'c'), ('c', 'a')]

e167_8 =
  ( ('a', 'b', 'c') & partsOf each %~ reverse
  ) == ('c', 'b', 'a')

e167_9 =
  ( [1, 2, 3, 4, 5, 6]
    & partsOf traversed %~ reverse . take 3
  ) == [3, 2, 1, 4, 5, 6]

e167_10 =
  ( ('a', 'b', 'c')
    & unsafePartsOf each %~ \ls -> (ls,) <$> ls
  ) == (("abc", 'a'), ("abc", 'b'), ("abc", 'c'))

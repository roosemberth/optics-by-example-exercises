module C12 where

import Control.Lens

e266_1 = view _1 ('a', 2)
e266_2 = view _1 ("abc", 123)
e267_3 = ("old", False) & _1 .~ "new"
e268_4 = view (_1 . _3) (('a', 'b', 'c'), 'd')
e268_5 = ("blue", Just (2 :: Int)) ^? _2 . _Just
e269_6 = ['a'..'z'] ^.. taking 5 folded
e269_7 = traverseOf both putStrLn ("one", "two")

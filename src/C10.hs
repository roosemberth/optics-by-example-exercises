{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TupleSections #-}
module C10 where

import Control.Lens
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text as T
import Control.Arrow (Arrow(..))
import Data.Char (toUpper, isUpper, toLower)
import Numeric.Lens
import Data.List (transpose, intercalate)
import Control.Exception (try, SomeException)
import GHC.IO.Unsafe (unsafePerformIO)
import Data.Either (isLeft)
import Data.Coerce (coerce)
import Data.List.NonEmpty (NonEmpty ((:|)))
import qualified Data.List as L

packed :: Iso' String T.Text
packed = iso to' from'
  where to'   = T.pack
        from' = T.unpack

e227_1 = (("Ay, caramba!" :: String) ^. packed) == ("Ay, caramba!" :: T.Text)
e228_1 = (("Good grief" :: T.Text) ^. from packed) == ("Good grief" :: String)

unpacked :: Iso' T.Text String
unpacked = from packed

e228_2 = ("Idol on a pedestal" :: String) & packed %~ T.replace "Idol" "Sand"
e229_1 = ("Lorem ipsum" :: T.Text) & from packed . traversed %~ toUpper
e230_1 = ("Fall", "Pride") ^. swapped == ("Pride", "Fall")

-- e234_1:
--
-- - Focus a Celsius temp in Fahrenheit: Iso, both contain the same information.
-- - Focus the last element of a list: Prism, the last element may not be there.
-- - View a JSON object as a haskell record: Prism, the json may not represent the hs record.
-- - Rotate the elements of a three tuple one ot the right: Iso, same information.
-- - Focus on the bits of an Int as Bools: Iso, transformation is bijective.
-- - Focusing an IntSet from a Set Int: Iso, transformation is bijective ignoring bottom.

e234_2a = ("Beauty", "Age") ^. swapped == ("Age", "Beauty")
e234_2b = 50 ^. from (adding 10) == 40
e234_2c = (0 & multiplying 4 +~ 12) == 3
e234_2d = (0 & adding 10 . multiplying 2 .~ 24) == 2
e234_2e = ([1, 2, 3] & reversed %~ drop 1) == [1, 2]
e234_2f = e == [3, 4, 1, 2]
  where e = view flipped (++) [1, 2] [3, 4]
e234_2g = [1, 2, 3] ^. reversed == [3, 2, 1]
e234_2 = and [e234_2a, e234_2b, e234_2c, e234_2d, e234_2e, e234_2f, e234_2g]

e234_2'a = e == [[2, 3], [20, 30]]
  where e = [[1, 2, 3], [10, 20, 30]] & involuted transpose %~ tail
e234_2'b = e == (32, "Hello" :: String)
  where switchCase c = if isUpper c then toLower c else toUpper c
        e = (32, "Hi" :: String)
          & _2 . involuted (fmap switchCase)
          .~ ("hELLO" :: String)

fahrenheit :: Iso' Double Double
fahrenheit = iso celsiusToF fahrenheitToC
  where celsiusToF c    = (c * (9/5)) + 32
        fahrenheitToC f = (f - 32) * 5/9

textToYamlList :: [T.Text] -> T.Text
textToYamlList = toYamlList ^. dimapping (mapping unpacked) packed
  where toYamlList xs = "- " <> intercalate "\n- " xs

e235_1a = e == ("egA" :: String, "Beauty" :: String)
  where e = ("Beauty", "Age" :: String) ^. mapping reversed . swapped
e235_1b = e == [False, True, False]
  where e = [True, False, True] ^. mapping (involuted not)
e235_1c = e == [False]
  where e = [True, False, True] & mapping (involuted not) %~ filter id
e235_1d = (show ^. dimapping id reversed) 1234 == "4321"

intNot :: Int -> Int
intNot = not ^. dimapping enum (from enum)

e235_2a = intNot 0 == 1
e235_2b = intNot 1 == 0
{-# NOINLINE e235_2c #-}
e235_2c = isLeft $ unsafePerformIO $ try @SomeException (pure $! intNot 2)

intNot' :: Int -> Int
intNot' = enum %~ not

newtype Email = Email { _email :: String }
  deriving Show

newtype UserID = UserID String
  deriving Show

e240_1 = coerce ("joe@example.com" :: String) :: Email
e240_2 = Email "joe@example.com" & coerced @_ @Email @String %~ reverse

makeLenses ''Email -- Generates 'email' using coerced.
e241_1 = Email "joe@example.com" & email %~ reverse

makeWrapped ''Email
e242_1 = Email "joe@example.com" & _Wrapped' %~ reverse
e243_1 = Email "joe@example.com" & _Wrapping' Email %~ reverse

e244 = ("Testing one two three" :: String) ^. reversed . from reversed

mapList :: Ord k => Iso' (M.Map k v) [(k, v)]
mapList = iso M.toList M.fromList

e244_1 = (v & from mapList %~ id) /= v
  where v = [(1, 1), (1, 1)] :: [(Integer, Integer)]

nonEmptyList :: Iso [a] [b] (Maybe (NonEmpty a)) (Maybe (NonEmpty b))
nonEmptyList = iso to from
  where from Nothing   = []
        from (Just ls) = ls ^.. traversed
        to []        = Nothing
        to (x:xs)    = Just (x :| xs)

-- e245_3: sorted cannot be a lawful iso because the element order is lost.

sorted :: Ord a => Iso' [a] [(Int, a)]
sorted = iso to' from'
  where to' xs = L.sortOn snd $ zip [0..] xs
        from' xs = snd <$> L.sortOn fst xs

e245_4 = (v & from sorted %~ id) /= v
  where v = (0,) <$> [1..10]

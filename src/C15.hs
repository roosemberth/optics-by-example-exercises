{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
module C15 where

import Control.Lens
import Data.Aeson
import Data.Aeson.Lens
import Text.RawString.QQ (r)
import qualified Data.Text as T
import Data.Ord (comparing)
import GHC.Generics (Generic)
import qualified Data.ByteString as BS
import qualified Data.Set as S

jsonObject :: String
jsonObject = [r|{
  "name": "Jack Sparrow",
  "rank": "Captain"
}|]

jsonArray :: String
jsonArray = [r|{
  "North",
  "East",
  "South",
  "West"
}|]

e292_1 = jsonObject ^? _Object
e292_2 = jsonArray ^? _Array

blackPearl :: String
blackPearl = [r|{
  "name": "Black Pearl",
  "crew": [{
    "name": "Jack Sparrow",
    "rank": "Captain"
  }, {
    "name": "Will Turner",
    "rank": "First Mate"
  }]
}|]

e294_1 :: Maybe T.Text
e294_1 = blackPearl ^? key "crew" . nth 0 . key "name" . _String

fleet :: String
fleet = [r|[
  {
    "name": "Black Pearl",
    "crew": [{
      "name": "Jack Sparrow",
      "rank": "Captain"
    }, {
      "name": "Will Turner",
      "rank": "First Mate"
    }]
  }, {
    "name": "Flying Dutchman",
    "crew": [{
      "name": "Davy Jones",
      "rank": "Captain"
    }, {
      "name": "Bootstrap Bill",
      "rank": "First Mate"
    }]
  }
]|]

e297_1 = fleet ^.. values
e297_2 = fleet ^.. values . key "name" . _String
e297_3 = fleet ^.. values . key "crew" . values . key "name" . _String
e297_4 = fleet
  ^@.. values
  . reindexed (view $ key "name" . _String) selfIndex
  <. key "crew" . values . key "name" . _String

cargo :: String
cargo = [r|{
  "emeralds": 327,
  "rubies": 480,
  "sapphires": 621,
  "opals": 92,
  "dubloons": 34
}|]

e298_1 = cargo ^@.. members . _Integer
e299_1 = maximumByOf (members . _Integer . withIndex) (comparing snd) cargo

e301_1 = fleet
  ^.. values
  . key "crew"
  . values
  . filteredBy (key "rank" . only "Captain")
  . key "name" . _String

data Creature = Creature
  { creatureName :: String
  , creatureSize :: Double
  } deriving stock (Generic, Show)
    deriving anyclass (ToJSON, FromJSON)

makeLensesFor [("creatureName", "cname"), ("creatureSize", "csize")] ''Creature

creatureSightings :: String
creatureSightings = [r|{
  "Arctic": [{
    "creatureName": "Giant Squid",
    "creatureSize": 45.2
  }],
  "Pacific": [{
    "creatureName": "Kraken",
    "creatureSize": 124.4
  }, {
    "creatureName": "Ogopogo",
    "creatureSize": 34.6
  }]
}|]

e304_1 = creatureSightings
  & members
  . values
  . _JSON' @_ @Creature
  . csize
  *~ 1.2

pods :: BS.ByteString
pods = [r|{
  "kind": "List",
  "apiVersion": "v1",
  "items": [
    {
      "kind": "Pod",
      "apiVersion": "v1",
      "metadata": {
        "name": "redis-h315w",
        "creationTimestamp": "2019-03-23T19:42:21Z",
        "labels": {
          "name": "redis",
          "region": "usa"
        }
      },
      "spec": {
        "containers": [
          {
            "name": "redis",
            "image": "redis",
            "ports": [
              {
                "name": "redis",
                "hostPort": 27017,
                "containerPort": 27017,
                "protocol": "TCP"
              }
            ],
            "resources": {
              "requests": {
                "cpu": "100m"
              }
            }
          }
        ]
      }
    },
    {
      "kind": "Pod",
      "apiVersion": "v1",
      "metadata": {
        "name": "web-4c5bj",
        "creationTimestamp": "2019-02-24T20:23:56Z",
        "labels": {
          "name": "web",
          "region": "usa"
        }
      },
      "spec": {
        "containers": [
          {
            "name": "web",
            "image": "server",
            "ports": [
              {
                "name": "http-server",
                "containerPort": 3000,
                "protocol": "TCP"
              }
            ],
            "resources": {
              "requests": {
                "cpu": "100m"
              }
            }
          }
        ]
      }
    }
  ]
}|]

e307_1 = pods ^? key "apiVersion" . _String
e307_2 = length $ pods
  ^.. key "items" . values . filteredBy (key "kind" . only "Pod")
e307_3 = pods
  ^.. key "items"
  . values
  . key "spec"
  . key "containers"
  . values
  . filtered (\c -> c ^? key "name" == c ^? key "image")
  . key "name"
  . _String
e307_4 = pods
  ^@.. key "items"
  . values
  . reindexed (^. key "metadata" . key "name" . _String) selfIndex
  <. key "spec"
   . key "containers"
   . values
   . key "ports"
   . values
   . key "containerPort"
   . _Integer
e307_5 = pods
  & key "items"
  . values
  . key "metadata"
  . key "labels"
  . members
  . _String
  %~ T.toUpper
e307_6 = pods
  & key "items"
  . values
  . key "spec"
  . key "containers"
  . values
  . key "resources"
  . key "requests"
  . _Object . at "memory"
  ?~ "256M"
e307_7 = ifoldlOf
  ( key "items"
  . values
  . key "metadata"
  . key "labels"
  . _Object . itraversed
  )
  (\i r a -> S.insert i r)
  S.empty
  pods
e307_7' = foldMapOf
  ( key "items"
  . values
  . key "metadata"
  . key "labels"
  . members
  . asIndex
  )
  S.singleton
  pods
e307_8 = pods
  & key "items"
  . values
  . key "spec"
  . key "containers"
  . values
  . key "ports"
  . values
  . _Object . at "hostPort"
  . filteredBy _Nothing
  ?~ _Number # 8080
e307_8' = pods
  & key "items"
  . values
  . key "spec"
  . key "containers"
  . values
  . key "ports"
  . values
  . _Object . at "hostPort"
  . filteredBy _Nothing
  . below _Integer
  ?~ 8080
e307_9 = iover
  ( key "items"
  . values
  . reindexed (^. key "metadata" . key "labels" . key "region" . _String)
      selfIndex
  <. key "spec"
   . key "containers"
   . values
   . key "name" . _String
  )
  (\i n -> i <> "-" <> n)
  pods
e307_9' = pods
  & key "items"
  . values
  . reindexed (^. key "metadata" . key "labels" . key "region" . _String)
      selfIndex
  <. key "spec"
   . key "containers"
   . values
   . key "name" . _String
  %@~ (\i n -> i <> "-" <> n)

{-# LANGUAGE BangPatterns #-}
{-# OPTIONS_GHC -fobject-code #-}
{-# OPTIONS_GHC -O2 #-}
module ThunksSharingLaziness where

-- Disclamer:
-- The code in this module is based on Edward Kmett's awswer on SO
-- (https://stackoverflow.com/a/3209189) and Joachim Breitner's talk
-- _Thunks, Sharing, Laziness: The Haskell Heap Visualized_
-- (https://www.youtube.com/watch?v=I4lnCG18TaY).

f :: (Int -> Int) -> Int -> Int
f mf 0 = 0
f mf n = max n $ mf (n `div` 2) + mf (n `div` 3) + mf (n `div` 4)

f_list :: [Int]
f_list = map (f faster_f) [0..]

faster_f :: Int -> Int
faster_f n = f_list !! n

data Tree a = Tree (Tree a) a (Tree a)

instance Functor Tree where
  fmap f (Tree l m r) = Tree (fmap f l) (f m) (fmap f r)

index' :: Tree a -> Int -> a
index' (Tree _ m _) 0 = m
index' (Tree l _ r) n = case (n - 1) `divMod` 2 of
  (q, 0) -> index' l q
  (q, 1) -> index' r q

nats :: Tree Int
nats = go 0 1
  where go !n !s = Tree (go l s') n (go r s')
          where l = n + s
                r = l + s
                s' = s * 2

f_tree :: Tree Int
f_tree = fmap (f fastest_f) nats

fastest_f :: Int -> Int
fastest_f = index' f_tree

-- Talk ends here. Hereon be my own experiments...

--import Control.Lens

--invertedIndex :: IndexedTraversal Int [a] [b] a b
--invertedIndex p ls = reindexed (l-1-) itraversed p ls
--  where l = length ls
--
--e1 = [1..20] ^@.. invertedIndex

--invertedIndex' :: IndexedTraversal Int [a] [b] a b
--invertedIndex' =
--  reindexed
--    (\(xs, i) -> length xs - 1 - i)
--    (selfIndex <.> traversed)

--e2 = [1..20] ^@.. invertedIndex'

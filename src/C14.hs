{-# LANGUAGE FunctionalDependencies #-}
module C14 where

import Control.Lens
import Control.Monad.IO.Class (MonadIO (..))
import Control.Monad.Reader (MonadReader (..), ReaderT (..))

newtype Person = Person
  { _personName :: String
  } deriving Show

newtype Pet = Pet
  { _petName :: String
  } deriving Show

newtype Serial = Serial
  { _serialName :: Int
  } deriving Show

makeFields ''Person
makeFields ''Pet
makeFields ''Serial

greetByName :: HasName r String => r -> IO ()
greetByName r = putStrLn $ "Hello " <> r ^. name <> "!"

type DatabaseUrl = String
type ServerInit = (String, Int)

data Env = Env
  { _serverCfg   :: ServerInit
  , _databaseUrl :: String
  } deriving Show

makeLenses ''Env

connectDB :: (MonadIO m, MonadReader DatabaseUrl m) => m ()
connectDB = do
  url <- ask
  liftIO $ putStrLn ("connecting to db at: " <> url)

initialize :: (MonadIO m, MonadReader ServerInit m) => m ()
initialize = do
  (host, port) <- ask
  liftIO $ putStrLn ("initializing server at: " <> host <> ":" <> show port)

e281_1 :: IO ()
e281_1 = flip runReaderT (Env ("example.com", 8000) "db.example.com") $ do
  magnify serverCfg initialize
  magnify databaseUrl connectDB

-- e284

data Env' = Env'
  { _envPortNumber :: Int
  , _envHostName :: String
  , _envDatabaseUrl :: String
  } deriving Show

makeFields ''Env

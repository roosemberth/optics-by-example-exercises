{-# LANGUAGE TupleSections #-}
{-# LANGUAGE ViewPatterns #-}
module C9 where

import Control.Lens
import qualified Data.Map as M
import qualified Data.Set as S
import qualified Data.Text as T
import Control.Arrow (Arrow(..))
import Data.Char
import Control.Lens.Extras (is)
import Data.List (stripPrefix, intercalate)
import Control.Monad (guard)

e196_1 = has _Right (Right 37)
e196_2 = isn't _Nothing (Just 12)

type Path = [String]
type Body = String

data Request
  = Post Path Body
  | Get Path
  | Delete Path
  deriving Show

makePrisms ''Request

e197_1 = Get ["users"] ^? _Get
e197_2 = Get ["users"] ^? _Post
e197_3 = Get ["users"] & _Get .~ ["posts"]
e197_4 = Post ["users"] "name: John" & _Post . _1 <>~ ["12345"]

e198_1 = review _Get ["posts"]
e198_2 = _Post # (["posts"], "My blog post")

e200_1 = "Freedom!" & _Cons %~ first toLower
e200_2 = "Freedom!" & _head .~ 'J'

e201_1 = isn't _Empty []
e201_2 = is _Empty M.empty
e201_3 = is _Empty [1, 2, 3]

e202_1 = "It's True that I ate 3 apples and 5 oranges"
  ^.. worded . _Show :: [Int]
e202_2 = "It's True that I ate 3 apples and 5 oranges"
  ^.. worded . _Show :: [Bool]

data ContactInfo
  = Email String
  | Telephone Int
  | Address String String String

makePrisms ''ContactInfo

e202_3 :: Prism' ContactInfo String
e202_3 = _Email
e202_4 :: Prism' ContactInfo Int
e202_4 = _Telephone
e202_5 :: Prism' ContactInfo (String, String, String)
e202_5 = _Address

e203_1 = e == Right 40
  where e = Right 35 & _Right +~ 5 :: Either () Int
e203_2 = e == ["Mind", "Power", "Soul", "Time"]
  where
    e = [Just "Mind", Just "Power", Nothing, Just "Soul", Nothing, Just "Time"]
      ^.. folded . _Just
e203_3 = e == r
  where
    e = [Just "Mind", Just "Power", Nothing, Just "Soul", Nothing, Just "Time"]
      & traversed . _Just <>~ " Stone"
    r =
      [ Just "Mind Stone"
      , Just "Power Stone"
      , Nothing
      , Just "Soul Stone"
      , Nothing
      , Just "Time Stone"
      ]
e203_4 = e == Left (Right False, "Eureka!")
  where e = (Left (Right True, "Eureka!") :: Either (Either () Bool, String) ())
          & _Left . _1 . _Right %~ not
e203_5 = e == ["Do", "Re", "Mi"]
  where e = _Cons # ("Do", ["Re", "Mi"])
e203_6 = True == isn't (_Show :: Prism' String Int) "not an int"

e203_7 = e == [1, 3]
  where e = (Just 1, Nothing, Just 3) ^.. each . _Just
e203_8 = e == "xzy"
  where e = _Cons # (('x', "yz") & _2 %~ reverse)
e203_9 = e == Left (Just (Right "do the hokey pokey"))
  where e = _Left @_ @() . _Just . _Right @() @_ # "do the hokey pokey"

_Just' :: Prism (Maybe a) (Maybe b) a b
_Just' = prism Just match
  where match Nothing  = Left Nothing
        match (Just v) = Right v

_Nothing' :: Prism' (Maybe a) ()
_Nothing' = prism' (const Nothing) match
  where match Nothing = Just ()
        match _       = Nothing

_Prefix :: String -> Prism' String String
_Prefix p = prism' (p <>) (stripPrefix p)

_Secure = _Prefix "https://"

e207_1 = "https://mybank.com" & _Secure <>~ "?accountNumber=12345"
e207_2 = "http://fakebank.com" & _Secure <>~ "?accountNumber=12345"

-- FizzBuzz! Iterate through the numbers 1 to 100
-- print Fizz if the number is divisible by 3
-- print Buzz if divisible by 5
-- print FizzBuzz if divisible by both
-- print the number if not divisible by either

dividesBy :: Integer -> Integer -> Bool
dividesBy n f = n `divMod` f & (== 0) . snd

e207_3_match
  :: Integer
  -> Prism (Either Integer String) (Either Integer String) Integer String
e207_3_match d = prism Right match
  where match (Left v) | v `dividesBy` d = Right v
        match v                        = Left v

e207_3 = fmap Left [1..100]
  & each . e207_3_match 15 .~ "fizzbuzz"
  & each . e207_3_match 5 .~ "buzz"
  & each . e207_3_match 3 .~ "fizz"
  & each . e207_3_match 1 %~ show
  & toListOf (each . _Right)

_Factor :: Integer -> Prism' Integer Integer
_Factor n = prism' (*n) (`mDiv` n)

mDiv :: Integer -> Integer -> Maybe Integer
mDiv a b = if r == 0 then Just f else Nothing
  where (f, r) = a `divMod` b

prismFizzBuzz :: Integer -> String
prismFizzBuzz n
  | has (_Factor 15) n = "fizzbuzz"
  | has (_Factor 5) n  = "buzz"
  | has (_Factor 3) n  = "fizz"
  | otherwise          = show n

e207_3' = [1..100] & each %~ prismFizzBuzz

-- | This Prism' is unlawful because we lost information on our way back...
_Tail :: Prism' [a] [a]
_Tail = prism' embed match
  where embed = id -- Wrong: Where are we going to get the missing elem from?
        match [] = Nothing
        match ls = Just (tail ls)

_ListCons :: Prism [a] [b] (a, [a]) (b, [b])
_ListCons = prism (uncurry (:)) match
  where match []     = Left []
        match (l:ls) = Right (l, ls)

_Cycles :: Integer -> Prism' String String
_Cycles n = prism' embed match
  where embed p  = foldOf each (replicate' n p)
        replicate' = replicate . fromIntegral
        match ls
          | embed particle == ls = Just particle
          | otherwise            = Nothing
          where particle = take pLen ls
                pLen = length ls `div` fromInteger n

-- Can't implement _Cycles': Where would we review
--  the number of cycles from?

-- e215_1
_Contains :: Ord a => a -> Prism' (S.Set a) (S.Set a)
_Contains a = prism' embed match
  where embed ss = S.singleton a <> ss
        match ss | S.member a ss = Just (S.delete a ss)
        match _                  = Nothing

e215_1a = Just (S.fromList [1, 3]) == e
  where e = S.fromList [1..3] ^? _Contains 2
e215_1b = Nothing == e
  where e = S.fromList [1..3] ^? _Contains 10
e215_1c = S.fromList [1, 2, 3, 10] == e
  where e = _Contains 10 # S.fromList [1..3]
e215_1d = S.fromList [1..3] == e
  where e = _Contains 2 # S.fromList [1..3]

-- _Contains is not lawful because insertion is idempotent:
e215_1' = ss /= v
  where ss     = S.fromList [1..3]
        Just v = (_Contains 1 # ss) ^? _Contains 1

_Singleton :: forall a. Prism' [a] a
_Singleton = prism' embed match
  where embed a   = [a]
        match [a] = Just a
        match _   = Nothing

-- e215_2: _Singleton is lawful because
-- - embedding after previewing yields back the list with one value:
--   preview only succeeds if list contains a single value
-- - previewing after embedding yields back the element cause the list only ever
--   has one value (and the preview will always succeed).
--  - The prism does not alter the big type: No bypass transformation.

e215_3_Bad_Singleton :: forall a. Prism' [a] a
e215_3_Bad_Singleton = prism' embed match
  where embed a = [a]
        match [] = Nothing
        match ls = Just (head ls)

-- e215_3_Bad_Singleton violates the first law.
e215_3 = ls /= e
  where ls     = [1..10]
        Just v = ls ^? e215_3_Bad_Singleton
        e      = e215_3_Bad_Singleton # v

-- The `Request` type of e216 was already created on line 18.

path :: Lens' Request Path
path = lens getter setter
  where
    getter (Post p _) = p
    getter (Get p)    = p
    getter (Delete p) = p
    setter (Post _ b) p = Post p b
    setter (Get _) p    = Get p
    setter (Delete _) p = Delete p

serve404 :: Request -> String
serve404 _ = "404 Not Found"

_PathPrefix :: String -> Prism' Request Request
_PathPrefix prefix = prism' embed match
  where embed r = r & path %~ (prefix :)
        match r
          | has (path . _head . only prefix) r = Just (r & path %~ drop 1)
        match _ = Nothing

safeTail :: [a] -> [a]
safeTail = tail & outside _Empty .~ const []

---------- Pretty fizzbuzz using `outside` :D
_Divisible :: Integer -> Prism' Integer Integer
_Divisible n = filtered (`dividesBy` n)
  where dividesBy n f = n `divMod` f & (== 0) . snd

fizzbuzz :: Integer -> String
fizzbuzz = show
  & outside (_Divisible 3)  .~ const "Fizz"
  & outside (_Divisible 5)  .~ const "Buzz"
  & outside (_Divisible 15) .~ const "FizzBuzz"
----------

userHandler :: Request -> String
userHandler r = "User handler! Remaining path: " <> intercalate "/" (r ^. path)

postsHandler :: Request -> String
postsHandler = serve404
  & outside _Post   .~ (\(_, body) -> "Create post with body: " <> body)
  & outside _Get    .~ (\path -> "Fetch post at path: " <> intercalate "/" path)
  & outside _Delete .~ (\path -> "Delete post at path: " <> intercalate "/" path)

serveRequest :: Request -> String
serveRequest = serve404
  & outside (_PathPrefix "users") .~ userHandler
  & outside (_PathPrefix "posts") .~ postsHandler
  & outside (_PathPrefix "posts" . _PathPrefix "index") .~ const "Post Index"

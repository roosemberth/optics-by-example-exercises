module C2 where

import Control.Lens
import Data.Char
import Control.Lens.Extras
import Data.Data
import Control.Monad (guard)

newtype Foo a = Foo a
  deriving stock (Data, Eq, Show)

e1 = (Foo @Integer 3, ()) & biplate @_ @Integer %~ (+ 1)

e2 = Foo @Integer 3 &
  partsOf (filtered (\(Foo n) -> even n))
  %~ fmap (\(Foo n) -> Foo $ n + 1)

e3 = (1, 2, 3)
  & each
    %%~ \n -> guard @Maybe (n < 5) >> pure n

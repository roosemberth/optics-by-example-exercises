{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE OverloadedStrings #-}
module C11 where

import Control.Lens
import Data.Tree
import qualified Data.Map as M
import Data.List.Lens
import Data.Foldable (Foldable(..))
import qualified Data.Text as T
import Data.Text.Lens (unpacked)

e247_1 = itoListOf itraversed ["Summer", "Fall", "Winter", "Spring"]
e247_2 = ["Summer", "Fall", "Winter", "Spring"] ^@.. itraversed

e247_3 = agenda ^@.. itraversed
  where agenda = M.fromList [("Monday", "Shopping"), ("Tuesday", "Swimming")]
e247_4 = ((True, "value") ^@? itraversed) == Just (True, "value")

e248_1 = (t ^@.. itraversed) == [([], "top"), ([0], "left"), ([1], "right")]
  where t = Node "top" [Node "left" [], Node "right" []]

agenda = M.fromList [ ("Monday", ["Shopping", "Yoga"])
                    , ("Saturday", ["Brunch", "Food coma"])
                    ]

e248_2 = (agenda ^@.. itraversed . itraversed) == r
  where r = [ (0, "Shopping" :: String)
            , (1, "Yoga")
            , (0, "Brunch")
            , (1, "Food coma")
            ]

e249_1 = (agenda ^@.. itraversed <. itraversed) == r
  where r = [ ("Monday", "Shopping")
            , ("Monday", "Yoga")
            , ("Saturday", "Brunch")
            , ("Saturday", "Food coma")
            ]

e249_2 = (agenda ^@.. itraversed <.> itraversed) == r
  where r = [ (("Monday",   0), "Shopping")
            , (("Monday",   1), "Yoga")
            , (("Saturday", 0), "Brunch")
            , (("Saturday", 1), "Food coma")
            ]

e250_1 = take 8 (agenda ^@.. itraversed <.> itraversed <.> itraversed) == r
  where r = [ (("Monday", (0, 0)), 'S')
            , (("Monday", (0, 1)), 'h')
            , (("Monday", (0, 2)), 'o')
            , (("Monday", (0, 3)), 'p')
            , (("Monday", (0, 4)), 'p')
            , (("Monday", (0, 5)), 'i')
            , (("Monday", (0, 6)), 'n')
            , (("Monday", (0, 7)), 'g')
            ]

showDayAndNumber :: String -> Int -> String
showDayAndNumber a b = a <> ": " <> show b

e251_1 = (agenda ^@.. icompose showDayAndNumber itraversed itraversed) == r
  where r = [ ("Monday: 0", "Shopping")
            , ("Monday: 1", "Yoga")
            , ("Saturday: 0", "Brunch")
            , ("Saturday: 1", "Food coma")
            ]

(.++) :: (Indexed String s t -> r)
      -> (Indexed String a b -> s -> t)
      -> Indexed String a b -> r
(.++) = icompose (\a b -> a ++ ", " ++ b)

populationMap :: M.Map String (M.Map String Int)
populationMap = M.fromList
  [ ("Canada", M.fromList [("Ottawa", 994837), ("Toronto", 2930000)])
  , ("Germany", M.fromList [("Berlin", 3748000), ("Munich", 1456000)])
  ]

e251_2 = (populationMap ^@.. itraversed .++ itraversed) == r
  where r = [ ("Canada, Ottawa", 994837)
            , ("Canada, Toronto", 2930000)
            , ("Germany, Berlin", 3748000)
            , ("Germany, Munich", 1456000)
            ]

e252_1a = e == [("streamResponse", False), ("useSSL", True)]
  where e = M.fromList [("streamResponse", False), ("useSSL", True)]
          ^@.. itraversed
e252_1b = e == [('a', 1), ('b', 2), ('c', 3), ('d', 4)]
  where e = (M.fromList [('a', 1), ('b', 2)], M.fromList [('c', 3), ('d', 4)])
          ^@.. each . itraversed
e252_1c = e == [('a', True), ('b', False)]
  where e = M.fromList [('a', (True, 1)), ('b', (False, 2))]
          ^@.. itraversed <. _1
e252_1d = e == r
  where r = [ ((0, "Roses"), 3)
            , ((0, "Tulips"), 5)
            , ((1, "Frogs"), 8)
            , ((1, "Goldfish"), 11)
            ]
        e = [ M.fromList [("Tulips", 5), ("Roses", 3)]
            , M.fromList [("Goldfish", 11), ("Frogs", 8)]
            ] ^@.. itraversed <.> itraversed
e252_1e = e == [10, 21, 32]
  where e = [10, 20, 30] & itraversed %@~ (+)
e252_1 = and [e252_1a, e252_1b, e252_1c, e252_1d, e252_1e]

e252_1f =
  itraverseOf
    itraversed
    (\i s -> putStrLn (replicate i ' ' <> s))
    ["one", "two", "three"]
e252_1g =
  itraverseOf_
    itraversed
    (\n s -> putStrLn $ show n <> ": " <> s)
    ["Go shopping", "Eat lunch", "Take a nap"]

ratings = M.fromList
  [ ("Dark Knight", 94)
  , ("Dark Knight Rises", 87)
  , ("Death of Superman", 92)
  ]

e254_1 = ['a'..'z'] ^.. itraversed . indices even == "acegikmoqsuwy"
e254_2 = ratings ^.. itraversed . indices (has (prefixed "Dark"))
e254_3 = ['a'..'z'] ^? itraversed . index 10
e254_4 = ratings ^? itraversed . index "Death of Superman"

exercises :: M.Map String (M.Map String Int)
exercises = M.fromList
  [ ("Monday"   , M.fromList [("pushups", 10), ("crunches", 20)])
  , ("Wednesday", M.fromList [("pushups", 15), ("handstands", 3)])
  , ("Friday"   , M.fromList [("crunches", 25), ("handstands", 5)])
  ]

e255_1a =
  ifoldlOf
    (traversed . itraversed . index "crunches")
    (\_ r c -> r + c)
    0
    exercises
e255_1b =
  ifoldlOf
    (itraversed . index "Wednesday" . traversed)
    (\_ r c -> r + c)
    0
    exercises
e255_1c = exercises ^@.. itraversed <. ix "pushups"

board = [ "XOO" :: String
        , ".XO"
        , "X.."
        ]

e255_2a = board ^@.. itraversed <.> itraversed
e255_2b = board & (itraversed <.> itraversed) . index (1, 0) .~ 'X'
e255_2c = board ^.. itraversed . itraversed . index 1
e255_2d = board ^.. itraversed . index 2 . traversed

data Board a =
  Board
    a a a
    a a a
    a a a
  deriving (Show, Foldable)

data Position = I | II | III
  deriving (Show, Eq, Ord)

testBoard :: Board Char
testBoard =
  Board
    'X' 'O' 'X'
    '.' 'X' 'O'
    '.' 'O' 'X'

slotsFold :: IndexedFold (Position, Position) (Board a) a
slotsFold = ifolding $ \board ->
  zip [(x, y) | y <- [I, II, III], x <- [I, II, III]]
    (toList board)

e257_1 = testBoard ^@.. slotsFold . indices ((== II) . snd)

slotsTraversal :: IndexedTraversal (Position, Position) (Board a) (Board b) a b
slotsTraversal p (Board
                    a1 b1 c1
                    a2 b2 c2
                    a3 b3 c3)
  = Board <$> indexed p (  I,   I) a1
          <*> indexed p ( II,   I) b1
          <*> indexed p (III,   I) c1
          <*> indexed p (  I,  II) a2
          <*> indexed p ( II,  II) b2
          <*> indexed p (III,  II) c2
          <*> indexed p (  I, III) a3
          <*> indexed p ( II, III) b3
          <*> indexed p (III, III) c3

printBoard :: Board Char -> IO ()
printBoard = itraverseOf_ slotsTraversal printSlot
  where
    printSlot (III, _) c = putStrLn [c]
    printSlot (_ , _) c = putStr [c]

e260_1 = ("hello" :: T.Text) ^@.. indexing each
e260_2 = ['a'..'c'] ^@.. itraversed
e260_3 = ['a'..'c'] ^@.. reindexed (*10) itraversed
e260_4 = ['a'..'c'] ^@.. reindexed show itraversed

e261 = [("Berry", 37), ("Veronica", 12)] ^@.. traversed . selfIndex <. _2

pair :: IndexedFold Bool (a, a) a
pair = ifolding $ \(a, b) -> [(False, a), (True, b)]

e261_1 = e == [(False, 'a'), (True, 'b')]
  where e = ('a', 'b') ^@.. pair

pair' :: IndexedTraversal Bool (a, a) (b, b) a b
pair' p (a, b) = (,) <$> indexed p False a <*> indexed p False b

oneIndexed :: IndexedTraversal Int [a] [b] a b
oneIndexed = reindexed (+1) itraversed

e261_2a = e == [(1, 'a'), (2, 'b'), (3, 'c'), (4, 'd')]
  where e = ['a'..'d'] ^@.. oneIndexed

invertedIndex :: IndexedTraversal Int [a] [b] a b
invertedIndex p ls = reindexed (l-1-) itraversed p ls
  where l = length ls

invertedIndex' :: IndexedTraversal Int [a] [b] a b
invertedIndex' =
  reindexed
    (\(xs, i) -> length xs - 1 - i)
    (selfIndex <.> traversed)

e261_2b = e == [(3, 'a'), (2, 'b'), (1, 'c'), (0, 'd')]
  where e = ['a'..'d'] ^@.. invertedIndex

chars :: IndexedTraversal Int T.Text T.Text Char Char
chars = unpacked . itraversed

e261_3a = e == [(0, 'b'), (1, 'a'), (2, 'n'), (3, 'a'), (4, 'n'), (5, 'a')]
  where e = ("banana" :: T.Text) ^@.. chars

charCoords :: IndexedTraversal' (Int, Int) String Char
charCoords = lined <.> itraversed

e261_3b = e == r
  where e = "line\nby\nline" ^@.. charCoords
        r = [ ((0,0),'l'),((0,1),'i'),((0,2),'n'),((0,3),'e')
            , ((1,0),'b'),((1,1),'y')
            , ((2,0),'l'),((2,1),'i'),((2,2),'n'),((2,3),'e')
            ]

e262_1 = [('a', True), ('b', False), ('c', True)] ^@.. itraversed . _1'
  where _1' = cloneIndexPreservingLens _1

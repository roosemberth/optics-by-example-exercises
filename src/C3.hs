module C3 where
import Control.Lens
import Control.Monad
import Control.Monad.RWS
import Control.Arrow ((&&&))
import Control.Lens.Unsound (lensProduct)

e1 :: Lens' (Bool, (Int, String)) Int
e1 = _2 . _1

e2 :: Lens' (Char, Int) Int
e2 = _2

-- e3: view, 

e3_1 = ('a', 4) & (e2 & view)
e3_2 = ('a', 4) & e2 %~ (+1)
e3_3 = ('a', 5) & e2 %%~ (\n -> guard (even n) >> Just n)

e4 = view lens ('a', 'b', 'c')
  where lens = _3

e5 = over _2 (*10) (False, 2)

e5_1 = (over :: ASetter' (Bool, b) b -> (b -> b) -> (Bool, b) -> (Bool, b))
         (_2 :: Lens' (Bool, Int) Int)
         ((*10) :: Int -> Int)
         ((False, 2) :: (Bool, Int))

data Ship = Ship
  { _name    :: String
  , _numCrew :: Int
  } deriving Show

e18_1 :: forall structure focus.
  -- Eq constraints so I can implement the expression
  (structure ~ (Bool, Int), focus ~ Int)
  => Lens' structure focus
e18_1 = _2

e18_2 :: forall getter setter b a.
  (getter ~ (b -> a), setter ~ (b -> a -> b))
  => getter -> setter -> Lens' b a
e18_2 = lens

{-
name :: Lens' Ship String
name = lens _name (\r f -> r{_name = f})

numCrew :: Lens' Ship Int
numCrew = lens _numCrew (\r f -> r{_numCrew = f})
-}

makeLenses ''Ship

purplePearl = Ship "Purple Pearl" 38

e20_1 = over numCrew (+3) purplePearl

data Potion
data Wand
data Book

data Inventory = Inventory
  { _wand   :: Wand
  , _book :: Book
  , _potions :: [Potion]
  }

makeLenses ''Inventory

e23_1a :: Lens' Inventory Wand
e23_1a = wand

e23_1b :: Lens' Inventory Book
e23_1b = book

e23_1c :: Lens' Inventory [Potion]
e23_1c = potions

-- e23_2
data Spuzz
newtype Chumble = Chumble Spuzz

gazork :: Functor f => (Spuzz -> f Spuzz) -> Chumble -> f Chumble
gazork = lens (\(Chumble s) -> s) (\_ s -> Chumble s) :: Lens' Chumble Spuzz

data Pet = Pet
  { _petName :: String
  , _petType :: String
  }

makeLenses ''Pet

getPetName :: Pet -> String
getPetName = view petName

-- Is it a lens? :D

second :: Lens' (a, b, c) b
second = _2

inMaybe :: Lens' (Maybe a) a
inMaybe = error "Impossible: One cannot always retrieve an 'a' from a Maybe."

left :: Lens' (Either a b) a
left = error "Impossible: One cannot always retrieve an 'a' from an Either."

-- e25_4: It is not a lens: One cannot always extract a 3rd element from a List.

e25_5 :: Lens' (Bool, a, a) a
e25_5 =
  lens
    (\(b, a1, a2) -> if b then a1 else a2)
    (\(b, a1, a2) a ->
      if b
      then (b, a, a2)
      else (b, a1, a)
    )
-- ^^ I can write it, but I don't know whether this is law-abiding...

data Err
  = ReallyBadError { _msg :: String }
  | ExitCode       { _code :: Int }
  deriving Eq

-- makeLenses ''Err

-- This is purposefully a broken lens for e36_2:
msg :: Lens' Err String
msg = lens getMsg setMsg
  where
    getMsg (ReallyBadError message) = message
    -- Hrmm, I guess we just return ""?
    getMsg (ExitCode _) = ""
    setMsg (ReallyBadError _) newMessage = ReallyBadError newMessage
    -- Nowhere to set it, I guess we do nothing?
    setMsg (ExitCode n) newMessage = ExitCode n

e25_6 = error
  "Impossible: One cannot always extract a particular direction of a sum."

e36_1 :: Lens' Int Int
e36_1 = lens id (+)

e36_1a = set e36_1 1 10 == 10
e36_1b = set e36_1 (set e36_1 1 10) 11 == 11

-- e36_2
-- `msg` violates the set-get law. Counterexample:
e36_2 = view msg (set msg "foo" (ExitCode 22)) == "foo"

-- To test the get-set law, let's evaluate both type varians:
e36_2a = set msg (view msg v1) v1 == v1 && set msg (view msg v2) v2 == v2
  where v1 = ExitCode 22
        v2 = ReallyBadError "baz"
-- For the purposes of this construction "baz" is any string and 22 is any
-- value. => `msg` respects the get-set law.

-- To test the set-set law, let's evaluate both type varians:
e36_2b = set msg "foo" (set msg "bar" v1) == set msg "foo" v1
      && set msg "foo" (set msg "bar" v2) == set msg "foo" v2
  where v1 = ExitCode 22
        v2 = ReallyBadError "baz"
-- For the purposes of this construction "foo", "bar" and "baz" are any not
-- necessarily equal Strings and 22 is any value.
-- => `msg` respects the set-set law.

msg2 :: Lens' Err String
msg2 = lens getMsg setMsg
  where
    getMsg (ReallyBadError message) = message
    getMsg (ExitCode _) = ""
    setMsg (ReallyBadError _) newMessage = ReallyBadError newMessage
    setMsg (ExitCode n) newMessage = ReallyBadError newMessage

-- e36_3
-- To test the set-get law, let's evaluate both type varians:
e36_3 = view msg2 (set msg2 "foo" (ExitCode 22)) == "foo"
  where v1 = ExitCode 22
        v2 = ReallyBadError "baz"
-- For the purposes of this construction "foo" and "baz" are any not necessarily
-- equal strings and 22 is any value. => `msg2` respects the set-get law.

-- `msg2` violates the set-get law. Counterexample:
e36_3a = set msg2 (view msg2 v1) v1 == v1 && set msg2 (view msg2 v2) v2 == v2
  where v1 = ExitCode 22
        v2 = ReallyBadError "baz"

-- To test the set-set law, let's evaluate both type varians:
e36_3b = set msg2 "foo" (set msg2 "bar" v1) == set msg2 "foo" v1
      && set msg2 "foo" (set msg2 "bar" v2) == set msg2 "foo" v2
  where v1 = ExitCode 22
        v2 = ReallyBadError "baz"
-- For the purposes of this construction "foo", "bar" and "baz" are any not
-- necessarily equal Strings and 22 is any value.
-- => `msg2` respects the set-set law.

-- This lens is useful because it allows us to 'get an idea' of whichever error
-- there is and display it to the user.
msg3 :: Lens' Err String
msg3 = lens getMsg setMsg
  where
    getMsg (ReallyBadError message) = message
    getMsg (ExitCode n) = show n
    setMsg (ReallyBadError _) newMessage = ReallyBadError newMessage
    setMsg (ExitCode n) newMessage = ReallyBadError newMessage

data Builder = Builder
  { _context :: [String]
  , _build :: [String] -> String
  }

-- This law-abiding lens is anything but intuitive!
e36_6 :: Lens' Builder String
e36_6 = lens get set
  where get Builder{..} = _build _context
        set b newVal | newVal == get b = b
        set _ newVal = Builder{_build = const newVal, _context = []}

data User = User
  { _firstName :: String
  , _lastName :: String
  , _email :: String
  } deriving Show

makeLenses ''User

-- e41_1
username :: Lens' User String
username = lens (view email) (flip $ set email)

fullName :: Lens' User String
fullName = lens uGet uSet
  where uGet u = view firstName u <> " " <> view lastName u
        uSet u name = set (lensProduct firstName lastName) (first, last) u
          where first = takeWhile (/= ' ') name
                last = drop 1 . dropWhile (/= ' ') $ name

data Time = Time
  { _hours :: Int
  , _minutes :: Int
  }

clamp :: Int -> Int -> Int -> Int
clamp minVal maxVal = min maxVal . max minVal

hours :: Lens' Time Int
hours = lens getter setter
  where
    getter (Time h _) = h
    setter (Time _ m) newHours = Time (clamp 0 23 newHours) m

mins :: Lens' Time Int
mins = lens getter setter
  where
    getter (Time _ m) = m
    setter (Time h _) newMinutes = Time h (clamp 0 59 newMinutes)

data ProducePrices = ProducePrices
  { _limePrice :: Float
  , _lemonPrice :: Float
  } deriving Show

{- -- e45_1
limePrice :: Lens' ProducePrices Float
limePrice = lens getter setter
  where
    getter = _limePrice
    setter pp u = if u < 0 then setter pp 0 else pp{_limePrice = u}

lemonPrice :: Lens' ProducePrices Float
lemonPrice = lens getter setter
  where
    getter = _lemonPrice
    setter pp u = if u < 0 then setter pp 0 else pp{_lemonPrice = u}
-}

-- | Limits the second value to keep them no further than 0.5 appart.
keepClose :: (Float, Float) -> (Float, Float)
keepClose (a, b) | abs (a - b) < 0.5 = (a, b)
keepClose (a, b) = if b > a then (a, a + 0.5) else (a, a - 0.5)

limePrice :: Lens' ProducePrices Float
limePrice = lens getter setter
  where
    getter = _limePrice
    setter pp@ProducePrices{_lemonPrice = lemonPrice} nextLimePrice =
      let (_limePrice, _lemonPrice) = keepClose (nextLimePrice, lemonPrice)
      in if nextLimePrice < 0
      then setter pp 0
      else pp{_limePrice, _lemonPrice}

lemonPrice :: Lens' ProducePrices Float
lemonPrice = lens getter setter
  where
    getter = _lemonPrice
    setter pp@ProducePrices{_limePrice = limePrice} nextLemonPrice =
      let (_lemonPrice, _limePrice) = keepClose (nextLemonPrice, limePrice)
      in if nextLemonPrice < 0
      then setter pp 0
      else pp{_limePrice, _lemonPrice}

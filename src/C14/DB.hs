{-# LANGUAGE FunctionalDependencies #-}
module C14.DB (HasDatabaseUrl(..), connectDB) where

import Control.Lens
import Control.Monad.Reader

type DatabaseUrl = String

newtype DbFields = DbFields
  { _dbFieldsDatabaseUrl :: DatabaseUrl
  }

makeFields ''DbFields

connectDB :: (MonadIO m, HasDatabaseUrl e DatabaseUrl, MonadReader e m) => m ()
connectDB = do
  url <- view databaseUrl
  liftIO $ putStrLn ("connecting to db at: " <> url)

{-# LANGUAGE FunctionalDependencies #-}
module C14.People where

import Control.Lens

data Person = Person
  { _name :: String
  , _favouriteFood :: String
  } deriving Show

makeClassy ''Person

{-# LANGUAGE FunctionalDependencies #-}
module C14.ServerInit (HasHostname(..), HasPortNumber(..)) where

import Control.Lens
import Control.Monad.Reader

data InitFields = InitFields
  { _hostname   :: String
  , _portNumber :: Int
  }

makeFieldsNoPrefix ''InitFields

initialize ::
  ( MonadIO m
  , HasHostname e String
  , HasPortNumber e Int
  , MonadReader e m
  ) => m ()
initialize = do
  port <- view portNumber
  host <- view hostname
  liftIO $ putStrLn ("initializing server at: " <> host <> ":" <> show port)

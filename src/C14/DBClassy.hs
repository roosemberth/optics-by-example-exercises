{-# LANGUAGE FunctionalDependencies #-}
module C14.DBClassy (DbConfig(..), HasDbConfig(..), connectDB) where

import Control.Lens
import Control.Monad.Reader

type DatabaseUrl = String

data DbConfig = DbConfig
  { _databaseUrl    :: DatabaseUrl
  , _maxConnections :: Int
  }

makeClassy ''DbConfig

connectDB :: (MonadIO m, HasDbConfig e, MonadReader e m) => m ()
connectDB = do
  url <- view databaseUrl
  numConnections <- view maxConnections
  liftIO $ putStrLn ( "connecting to db at: "
                    <> url
                    <> " with max connections: "
                    <> show numConnections
                    )

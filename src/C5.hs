module C5 where

import Control.Lens
import Control.Lens.Unsound (lensProduct)
import Data.Char (toUpper)

data Payload = Payload
  { _weightKilos :: Int
  , _cargo :: String
  } deriving Show
makeLenses ''Payload

newtype Ship = Ship
  { _payload :: Payload
  } deriving Show
makeLenses ''Ship

serenity :: Ship
serenity = Ship $ Payload 50000 "Livestock"

e65_1 = serenity ^. payload . cargo
e66_1 = serenity & payload . cargo .~ "Medicine"

e68_1 = serenity
  & payload . cargo .~ "Medicine"
  & payload . weightKilos .~ 100
  & payload . weightKilos *~ 10

newtype Thermometer = Thermometer
  { _temperature :: Int
  } deriving Show
makeLenses ''Thermometer

e72_1 = Thermometer 20 & temperature <+~ 15
e72_2 = Thermometer 20 & temperature <<+~ 15

data Gate = Gate
  { _open :: Bool
  , _oilTemp :: Float
  } deriving (Show, Eq)
makeLenses ''Gate

data Army = Army
  { _archers :: Int
  , _knights :: Int
  } deriving (Show, Eq)
makeLenses ''Army

data Kingdom = Kingdom
  { _name :: String
  , _army :: Army
  , _gate :: Gate
  } deriving (Show, Eq)
makeLenses ''Kingdom

duloc =
  Kingdom
    "Duloc"
    (Army 22 14)
    (Gate True 10)

goalA =
  Kingdom
    "Duloc: a perfect place"
    (Army 22 42)
    (Gate False 10)

goalB =
  Kingdom
    "Dulocinstein"
    (Army 17 26)
    (Gate True 100)

goalC =
  ("Duloc: Home"
  , Kingdom
      "Duloc: Home of the talking Donkeys"
      (Army 22 14)
      (Gate True 5)
  )

e74_1a k = k
  & name <>~ ": a perfect place"
  & army . knights +~ 28
  & gate . open &&~ False

e74_1b k = k
  & name .~ "Dulocinstein"
  & army . lensProduct archers knights .~ (17, 26)
  & gate . lensProduct open oilTemp .~ (True, 100)

e74_1c k = k
  & name .~ "Duloc: Home"
  & army . lensProduct archers knights .~ (22, 14)
  & gate . oilTemp .~ 5
  & name <<<>~ " of the talking Donkeys"

e74_1' =
     e74_1a duloc == goalA
  && (duloc & e74_1b . e74_1a) == goalB
  && (duloc & e74_1c . e74_1b . e74_1a) == goalC

e74_2a = (False, "opossums") & _1 ||~ True
e74_2b = 2 & id *~ 3
e74_2c = ((True, "Dudley"), 55.0)
  & _1 . _2 <>~ " - the worst"
  & _2 -~ 15
  & _2 //~ 2
  & _1 . _2 %~ map toUpper
  & _1 . _1 &&~ False

e74_2 =
     e74_2a == (True, "opossums")
  && e74_2b == 6
  && e74_2c == ((False, "DUDLEY - THE WORST"), 20.0)

e74_3 = (.~)

-- Why does this not type-check when Using Lens instead of ASetter?
e74_4 :: ASetter s t a b -> (a -> b) -> s -> t
e74_4 = (%~)

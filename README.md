# Optics by example - Notes & Exercises

This repository is my own journey studying [Chris Penner's
_Optics By Example_ Book](https://leanpub.com/optics-by-example).

It's got a [nix flake](https://nixos.wiki/wiki/Flakes) for convenience so one
can 'simply' run `nix develop` and have a working `cabal repl` (plus hls and
commodity tools).

### Thunks, Sharing, Laziness

I found an interesting performance 'feature' on chapter 11, more precisely:

```haskell
invertedIndex :: IndexedTraversal Int [a] [b] a b
invertedIndex p ls = reindexed (l-1-) itraversed p ls
  where l = length ls

invertedIndex' :: IndexedTraversal Int [a] [b] a b
invertedIndex' =
  reindexed
    (\(xs, i) -> length xs - 1 - i)
    (selfIndex <.> traversed)
```

```
ghci> last $ [1..10000000] ^@.. invertedIndex
(0,10000000)
(3.33 secs, 4,401,050,408 bytes)
ghci> last $ [1..10000000] ^@.. invertedIndex'
(0,10000000)
(2.85 secs, 7,441,046,800 bytes)
```

I got curious and in my journey to understanding, I ended up taking a small
digression into Joachim Breitner's talk [Thunks, Sharing, Laziness:
The Haskell Heap Visualized](https://www.youtube.com/watch?v=I4lnCG18TaY).

I ended up discovering this great tool `GHC.Vis.vis`, which I added to the
project to study similar effects.

At the end, turns out the performance issue can be 'simply' explained by GHC
specializing code for one, while carrying the typeclass dictionaries for the
other, but the journey is the most important (at least to me).

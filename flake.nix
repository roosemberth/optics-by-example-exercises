{
  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = inputs: inputs.flake-utils.lib.eachDefaultSystem (system:
    with builtins;
    let
      pkgs = import inputs.nixpkgs { inherit system; };
      haskellDeps = drv: concatLists (attrValues drv.getCabalDeps);
      hs = pkgs.haskellPackages;
      obe = hs.callCabal2nix "obe" ./. {};
    in
    {
      packages = {
        inherit obe;
        default = obe;
      };

      devShells.default = pkgs.mkShell {
        nativeBuildInputs = [
          (hs.ghcWithPackages (ps: haskellDeps obe))
          hs.cabal-install
          hs.haskell-language-server
          hs.hpack
          pkgs.clang
        ];

        shellHook = ''
          # Fix GLib-GIO-ERROR missing 'org.gtk.Settings.FileChooser' on GHC.Vis.vis
          export XDG_DATA_DIRS="${pkgs.gtk3}/share/gsettings-schemas/${pkgs.gtk3.name}:$XDG_DATA_DIRS"
        '';
      };
    }
  );
}
